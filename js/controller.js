angular.module('githubApp', [
        'app.directive.topList',
        'app.services.repoServices'
])
.controller('MainCtrl', ['$scope', '$http', 'repoServices', function MainCtrl($scope, $http, repoServices) {

    $scope.repos;
    $scope.selectedRepo = {};
    $scope.selectedRepoCommits = [];
    $scope.selectedRepoComments = [];
    $scope.selectedRepoForks = [];
    $scope.orgName = "Netflix";
    $scope.starGazerRepos = [];
    $scope.forkedRepos = [];
    $scope.leastOpenIssuesRepos = [];
    $scope.mostOpenIssuesRepos = [];
    $scope.greatestRepos = [];
    $scope.mostUsedLanguages = [];
    $scope.showErrorMessage = false;

    $scope.getReposForOrgName = function () {
        var promise = repoServices.getReposForOrgName($scope.orgName);
        promise.then(function(response) {
            $scope.repos = response.data;
            //Run sorting analytics
            $scope.getTop10StargazerRepos($scope.repos, 10);
            $scope.getTop10ForkedRepos($scope.repos, 10);
            $scope.getTop10LeastIssuesRepos($scope.repos, 10);
            $scope.getTop10MostIssuesRepos($scope.repos, 10);
            $scope.getGreatestRepos($scope.repos, 3);
            $scope.getMostUsedLanguages($scope.repos, 10);
        });
    };

    $scope.getCommitsForRepo = function(repo, pagination) {
        var promise = repoServices.getCommitsForRepo(repo, pagination);
        promise.then(function(response) {
            if(response.pagination) {
                $scope.selectedRepoCommits.pagination = response.pagination;
            }
            $scope.selectedRepoCommits = $scope.selectedRepoCommits.concat(response.data);
        })
    };

    $scope.getForksForRepo = function(repo, pagination) {
        var promise = repoServices.getForksForRepo(repo, pagination);
        promise.then(function(response) {
            if(response.pagination) {
                $scope.selectedRepoForks.pagination = response.pagination;
            }
            $scope.selectedRepoForks = $scope.selectedRepoForks.concat(response.data);
        })
    };

    $scope.getCommentsForRepo = function(repo, pagination) {
        var promise = repoServices.getCommentsForRepo(repo, pagination);
        promise.then(function(response) {
            if(response.pagination) {
                $scope.selectedRepoComments.pagination = response.pagination;
            }
            $scope.selectedRepoComments = $scope.selectedRepoComments.concat(response.data);
        })
    };

    $scope.sortRepo = function(collection, field, order, count) {
        var orderedCollection = _.orderBy(collection,field, order);
        if(count <= orderedCollection.length)
            return orderedCollection.slice(0,count);
        else
            return orderedCollection
    };

    $scope.countValuesInRepos = function(collection, field, order, count) {
        var fieldCount = {};
        collection.map(function(obj) {
            return {field: obj[field], project: obj['name']};
        }).map(function(element) {
            if(fieldCount[element.field]) {
                var obj = fieldCount[element.field];
                obj.count += 1;
                obj.project += ", " + element.project;
                fieldCount[element.field] = obj;
            } else {
                var obj = {};
                obj.count = 1;
                obj.project = "";
                obj.project = element.project;
                fieldCount[element.field] = obj;
            }
        });

        var sortable = [];
        for (var field in fieldCount)
            sortable.push([field, fieldCount[field]])

            if(order == 'desc') {
                sortable.sort(function(a, b) {return b[1].count - a[1].count});
            } else if(order == 'asc') {
                sortable.sort(function(a, b) {return a[1].count - b[1].count});
            }
        if(count <= sortable.length)
            return sortable.splice(0, count);
        else
            return sortable;
    }

    $scope.setRepo = function(repo) {
        $scope.selectedRepo = repo;
        $scope.getCommitsForRepo($scope.selectedRepo);
        $scope.getForksForRepo($scope.selectedRepo);
        $scope.getCommentsForRepo($scope.selectedRepo);
    };

    $scope.getTop10StargazerRepos = function(repos, count) {
        $scope.starGazerRepos = $scope.sortRepo(repos, ["stargazers_count"], ["desc"], count);
    };

    $scope.getTop10ForkedRepos = function(repos, count) {
        $scope.forkedRepos = $scope.sortRepo(repos, ["forks_count"], ["desc"], count);
    };

    $scope.getTop10LeastIssuesRepos = function(repos, count) {
        $scope.leastOpenIssuesRepos = $scope.sortRepo(repos, ["open_issues"], ["asc"], count);
    };

    $scope.getTop10MostIssuesRepos = function(repos, count) {
        $scope.mostOpenIssuesRepos = $scope.sortRepo(repos, ["open_issues"], ["desc"], count);
    };

    $scope.getGreatestRepos = function(repos, count) {
        $scope.greatestRepos = $scope.sortRepo(repos, ["stargazers_count", "open_issues", "forks"], ["desc", "asc", "desc"], count);
    };

    $scope.getMostUsedLanguages = function(repos, count) {
        $scope.mostUsedLanguages = $scope.countValuesInRepos(repos, "language", "desc", 5);
    };

    $scope.init = function() {
        if($scope.orgName) {
            $scope.getReposForOrgName();
        }
    };

    $scope.init();
}]);
