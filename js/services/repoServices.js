angular.module('app.services.repoServices', [])
    .service("repoServices", function($http, $q) {
        var DEFAULT_PAGESIZE = 100;

        this.getReposForOrgName = function(orgName, pagination) {
            var url = 'https://api.github.com/orgs/' + orgName + '/repos';
            return httpGet(url, pagination);
        };

        this.getCommitsForRepo = function(repo, pagination) {
            var url = repo.commits_url.replace('{/sha}', '');
            return httpGet(url, pagination);
        };

        this.getForksForRepo = function(repo, pagination) {
            var url = repo.forks_url;
            return httpGet(url, pagination);
        };

        this.getCommentsForRepo = function(repo, pagination) {
            var url = repo.comments_url.replace('{/number}', '');
            return httpGet(url, pagination);
        };

        function httpGet(url, pagination) {
            if(!pagination) {
                pagination = "?page=1&per_page=" + DEFAULT_PAGESIZE;
            }
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: url + pagination
            }).then(function successCallback(response) {
                var paginationParams = "";
                if(response.headers()) {
                    if(response.headers().link) {
                        var link = response.headers().link;
                        paginationParams = link.substring(link.indexOf('?page'), link.indexOf(">;"));
                        response.pagination = paginationParams;
                    }
                }
                deferred.resolve(response);
            }, function errorCallback(response) {
                alert("Error getting data from: " + url);
            });
            return deferred.promise;
        }

    });