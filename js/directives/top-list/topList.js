angular.module('app.directive.topList', [])
    .directive('topList', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/topList.html',
            scope: {
                tableTitle: "@",
                orgName: "=",
                tableDescription: "@",
                tableHeader: "@",
                tableColumns: "@",
                dataRepos: "="
            },
            transclude: true,
            link: function($scope) {
                $scope.setDataRepos = function(dataRepos) {
                    $scope.dataRepos = dataRepos;
                };
                if($scope.tableHeader && $scope.tableColumns) {
                    $scope.tableHeaderArray = $scope.tableHeader.split(",");
                    $scope.tableColumnsArray = $scope.tableColumns.split(",");
                    console.log($scope.tableHeaderArray);
                    console.log($scope.tableColumnsArray);
                    console.log($scope.dataRepos);
                } else {
                    console.log("error in topList directive, undefined table-header and table-columns.")
                }
            }
        }
    });
