describe('This sortRepo function', function() {
    var scope, httpBackend, createController;

    beforeEach(module('githubApp'));

    beforeEach(inject(function($rootScope, $httpBackend, $controller) {
      httpBackend = $httpBackend;
      scope = $rootScope.$new();

      createController = function() {
          return $controller('MainCtrl', {
              '$scope': scope
          });
      };

    }));

    it('should work', function() {
        expect(true).toBe(true);
    });

    it('should sort collection by name in descending order', function() {
      var controller = createController();
      var collection = [
        {
          name:"andy",
          age: 10
        },
        {
          name:"charlie",
          age: 9
        },
      ];

      var sortedCollection = scope.sortRepo(collection, ['name'], ['desc']);
      expect(sortedCollection[0].name).toBe("charlie");
    });

    it('should sort collection by name in ascending order', function() {
      var controller = createController();
      var collection = [
        {
          name:"andy",
          age: 10
        },
        {
          name:"charlie",
          age: 9
        },
      ];

      var sortedCollection = scope.sortRepo(collection, ['name'], ['asc']);
      expect(sortedCollection[0].name).toBe("andy");
    });

    it('should sort collection by age in descending order', function() {
      var controller = createController();
      var collection = [
        {
          name:"andy",
          age: 10
        },
        {
          name:"charlie",
          age: 9
        },
      ];

      var sortedCollection = scope.sortRepo(collection, ['age'], ['desc']);
      expect(sortedCollection[0].age).toBe(10);
    });

    it('should sort collection by age in ascending order', function() {
      var controller = createController();
      var collection = [
        {
          name:"andy",
          age: 10
        },
        {
          name:"charlie",
          age: 9
        },
      ];

      var sortedCollection = scope.sortRepo(collection, ['age'], ['asc']);
      expect(sortedCollection[0].name).toBe("charlie");
    });

    it('should return a collection sorted by repo stargazers_count in desc order', function() {
      var controller = createController();
      jasmine.getJSONFixtures().fixturesPath='base/test/fixtures';
      var collection = getJSONFixture('repos.json');

      var sortedCollection = scope.sortRepo(collection, ['stargazers_count'], ['desc']);
      expect(sortedCollection[0].name).toBe("Hystrix");
    });

    it('should return a collection sorted by repo forks_count in desc order', function() {
      var controller = createController();
      jasmine.getJSONFixtures().fixturesPath='base/test/fixtures';
      var collection = getJSONFixture('repos.json');

      var sortedCollection = scope.sortRepo(collection, ['forks_count'], ['desc']);
      expect(sortedCollection[0].name).toBe("Hystrix");
    });


    it('should return a collection sorted by repo open_issues in asc order', function() {
      var controller = createController();
      jasmine.getJSONFixtures().fixturesPath='base/test/fixtures';
      var collection = getJSONFixture('repos.json');

      var sortedCollection = scope.sortRepo(collection, ['open_issues'], ['asc']);
      expect(sortedCollection[0].name).toBe("frigga");
    });

    it('should return a collection sorted by language and count of projects in desc order', function() {
      var controller = createController();
      jasmine.getJSONFixtures().fixturesPath='base/test/fixtures';
      var collection = getJSONFixture('repos.json');

      var sortedCollection = scope.countValuesInRepos(collection, "language", "desc", 5);
      expect(sortedCollection[0][0]).toBe("Java");
    });

})
